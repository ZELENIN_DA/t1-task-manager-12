package ru.t1.dzelenin.tm.constant;

public final class CommandConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_SHOW_BY_ID = "project-show-id";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-index";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-index";

    public static final String PROJECT_REMOVE_BY_ID = "project-delete-id";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-delete-index";

    public static final String PROJECT_START_BY_ID = "project-start-id";

    public static final String PROJECT_START_BY_INDEX = "project-start-index";

    public static final String PROJECT_COMPLETE_BY_ID = "project-complete-id";

    public static final String PROJECT_COMPLETE_BY_INDEX = "project-complete-index";

    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-id";

    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-index";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_SHOW_BY_ID = "task-show-id";

    public static final String TASK_SHOW_BY_INDEX = "task-show-index";

    public static final String TASK_UPDATE_BY_ID = "task-update-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-index";

    public static final String TASK_REMOVE_BY_ID = "task-delete-id";

    public static final String TASK_REMOVE_BY_INDEX = "task-delete-index";

    public static final String TASK_START_BY_ID = "task-start-id";

    public static final String TASK_START_BY_INDEX = "task-start-index";

    public static final String TASK_COMPLETE_BY_ID = "task-complete-id";

    public static final String TASK_COMPLETE_BY_INDEX = "task-complete-index";

    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-id";

    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-index";

    private CommandConst() {
    }

}
