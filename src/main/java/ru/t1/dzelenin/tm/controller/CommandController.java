package ru.t1.dzelenin.tm.controller;

import ru.t1.dzelenin.tm.api.ICommandController;
import ru.t1.dzelenin.tm.api.ICommandService;
import ru.t1.dzelenin.tm.model.Command;
import ru.t1.dzelenin.tm.util.FormatUtil;

import static java.lang.Runtime.getRuntime;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = getRuntime().maxMemory();
        final String maxMemoryFormat = (FormatUtil.formatBytes(maxMemory));
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? ("no limit") : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Daniil Zelenin");
        System.out.println("email: dzelenin@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
    }
}
