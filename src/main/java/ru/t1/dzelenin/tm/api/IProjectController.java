package ru.t1.dzelenin.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();
}
