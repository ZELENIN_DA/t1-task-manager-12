package ru.t1.dzelenin.tm.api;

import ru.t1.dzelenin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task tasks);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}
