package ru.t1.dzelenin.tm.api;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    void remove(Project project);

    Project changeProjectStatusIndex(Integer index, Status status);

    Project changeProjectStatusId(String id, Status status);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);
}
